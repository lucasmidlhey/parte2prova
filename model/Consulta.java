package model;
	import model.Medico;
	import model.Paciente;
	class Consulta {
	
	private String tipoConsulta;
	 Paciente paciente;
	private int preco;
	 Medico medico;
	private String diagnostico;
	private String horario;

	public Consulta (Medico medico,Paciente paciente){
		this.paciente = paciente;
		this.medico = medico;
	}
	public void setTipoConsulta(String tipoConsulta){
		this.tipoConsulta = tipoConsulta;
	}
	public String getTipoConsulta(){
		return tipoConsulta;
	}
	public void setpreco(int preco){
		this.preco = preco;
	}
	public int getpreco(){
		return preco;
	}
	public void setDiagnostico(String diagnostico){
		this.diagnostico = diagnostico;
	}
	public String getDiagnostico(){
		return diagnostico;
	}
	public void setHorario(String horario){
		this.horario = horario;
	}
	public String getHorario(){
		return horario;
	}





}
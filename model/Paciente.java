package model;

public class Paciente extends Usuario{
	
	private String sintomas;
	private int idade;
	public Paciente(String umNome) {
		super(umNome);
	}
	
	
	public void setSintomas(String sintomas){
		this.sintomas = sintomas;
	}
	public String getSintomas(){
		return sintomas;
	}
	
	public void setIdade(int idade){
		this.idade = idade;
	}
	public int getIdade(){
		return idade;
	}

	
}
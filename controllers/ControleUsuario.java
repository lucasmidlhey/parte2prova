package controllers;

import java.util.ArrayList;

import model.Medico;
import model.Paciente;
import model.Usuario;
public class ControleUsuario {

	
 ArrayList<Paciente> listaPaciente;
 ArrayList<Medico> listaMedico;
ArrayList<Usuario> listaUsuario;
	public ControleUsuario() {
		
		listaPaciente = new ArrayList<Paciente>();
	}

	
	public void mostrarPaciente(){
		for (Paciente umPaciente : listaPaciente){
			
			
			System.out.println("Nome: " + umPaciente.getNome()
				+ "\nSintomas: " + umPaciente.getSintomas()
				+ "\nIdade:"+ umPaciente.getIdade());
				
		}
		
	}
	public void mostrarMedico(){
		for (Medico umMedico : listaMedico){
			System.out.println("Nome: " + umMedico.getNome()
				+ "\nSintomas: " + umMedico.getEspecialidade()
				+ "\nIdade:"+ umMedico.getLocalTrabalho());
				
		}
		
	}

}